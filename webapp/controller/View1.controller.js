sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/Button",
	"sap/m/Dialog",
	"sap/m/Text",
	"sap/m/MessageToast",
	"sap/m/TextArea",
	"sap/m/Label"
], function(Controller, Button, Dialog, Text, MessageToast, TextArea, Label) {
	"use strict";

	var SecondClass = Controller.extend("Second_Class.controller.View1", {

		onInit: function() {

		},

		onSliderMoved: function(event) {

		},

		onMessageDialogPress: function(oEvent) {
			var dialog = new Dialog({
				title: "測試Dialog Message BOX",
				type: "Message",
				content: new Text({
					text: "測試Dialog Message BOX"
				}),
				beginButton: new Button({
					text: "確定",
					press: function() {
						dialog.close();
					}
				}),
				afterClose: function() {
					dialog.destroy();
				}
			});

			dialog.open();
		},

		onSubmitDialog: function() {
			var dialog = new Dialog({
				title: "Confirm",
				type: "Message",
				content: [
					new Label({
						text: "測試填寫Dialog Message BOX",
						labelFor: "submitDialogTextarea"
					}),
					new TextArea("submitDialogTextarea", {
						liveChange: function(oEvent) {
							var sText = oEvent.getParameter("value");
							var parent = oEvent.getSource().getParent();

							parent.getBeginButton().setEnabled(sText.length > 0);
						},
						width: "100%",
						placeholder: "Add note (required)"
					})
				],
				beginButton: new Button({
					text: "送出",
					enabled: false,
					press: function() {
						var sText = sap.ui.getCore().byId("submitDialogTextarea").getValue();
						MessageToast.show("填寫文字是: " + sText);
						dialog.close();
					}
				}),
				endButton: new Button({
					text: "取消",
					press: function() {
						dialog.close();
					}
				}),
				afterClose: function() {
					dialog.destroy();
				}
			});

			dialog.open();
		},

		onSearch: function(event) {
		//	var item =  sap.ui.getCore().byId("searchField");
			var oView = this.getView();
				MessageToast.show("查詢條件是: " +  oView.byId("searchField").getValue());
		},
	});

	return SecondClass;

});